import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4

ApplicationWindow {
    id: window
    visible: true

    /* Window Variables (can be modified from python) */
    // Title info
    property var intMinWidth: 300
    property var intMinHeight: 250
    property var intMaxWidth: 1000  // might not want to use these
    property var intMaxHeight: 1000  // might not want to use these
    // Current state boolean
    property var boolState: false
    // Audio device list
    property var arrAudioDeviceList: ['None']
    property var arrComDeviceList: ['None']

    /* Alias for easy access outside of QML */
    property alias aliasText: idText.text
    property alias aliasBoxColor: idColorBox.color

    /* SIGNALS */
    signal sigPttClicked( bool state )
    signal sigToggleClicked()
    signal sigRefreshList()
    signal sigDeviceChange( int index )

    /* Window Settings based on variables above */
    title: "PTT App"
    width: intMinWidth
    height: intMinHeight

    ColumnLayout {
        anchors.fill: parent
        Layout.minimumWidth: intMinWidth
        Layout.minimumHeight: intMinHeight

        spacing: 10

        Rectangle {
            id: idColorBox
            width: 20; height:20

            Layout.alignment: Qt.AlignHCenter

            color: "red"
        }

        /**/
        RowLayout {
            Layout.alignment: Qt.AlignHCenter

            Text {
                text: "Audio Devices: "
            }

            ComboBox {
                id: idDeviceCombo
                model: window.arrAudioDeviceList

                onActivated: {
                    sigDeviceChange( index )
                }
            }
        } /*END RowLayout (for device list)*/

        /**/
        RowLayout {
            Layout.alignment: Qt.AlignHCenter

            Text {
                text: "COM Devices: "
            }

            ComboBox {
                model: window.arrComDeviceList
            }
        } /*END RowLayout (for COM device list)*/

        TextField {
            id: idText

            Layout.alignment: Qt.AlignHCenter
            text: "Nothing Yet.."
            readOnly: true
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter

            /* Button to simulate Push-To-Talk feature */
            Button {
                id: idButtonPtt
                objectName: "nameBPtt"
                text: "PTT"

                MouseArea {
                    anchors.fill: parent
                    onPressed: {
                        sigPttClicked( true )
                    }
                    onReleased: {
                        sigPttClicked( false )
                    }
                }
            }

            /* Button to handle microphone toggling status */
            Button {
                id: idButtonToggle
                objectName: "nameBToggle"
                text: "Toggle"

                onClicked: {
                    sigToggleClicked()
                }
            }
        } /*END RowLayout (for buttons) */

        /* Create a checkbox to handle "window stays on top" status*/
        CheckBox {
            id: idStayOnTop

            Layout.alignment: Qt.AlignHCenter

            text: "Always On Top"
            checked: false
        }
    } /*END ColumnLayout*/

    /* ------- SLOT and SIGNAL connections ---------- */

    /* When the QML document is completed, run these sequences */
    Component.onCompleted: {
        // Connect signals to local slots
        idStayOnTop.clicked.connect( setWindowFlags )

        // Set the default window status
        window.setWindowFlags()
    }

    /**
     * Slot used to handle setting the "window stays on top" status
     */
    function setWindowFlags() {
        if ( idStayOnTop.checked ) {
            window.flags = window.flags | Qt.WindowStaysOnTopHint
        } else {
            window.flags = window.flags & ~Qt.WindowStaysOnTopHint
        }
    } /*END funtion setWindowFlags() */

    /**
     * Used to set the text or color box to the current status
     */
    function setStatus() {
        if( window.boolState ) {
            idColorBox.color = "green"
        } else {
            idColorBox.color = "red"
        }
    } /*END function setStatus()*/
}/*END ApplicationWindow*/
