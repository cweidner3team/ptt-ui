import sys, os
from subprocess import call
import csv

gSystem = 'unknown'
if sys.platform.startswith('win'):
    gSystem = 'windows'
    os.environ["PATH"] += os.pathsep + os.path.dirname(os.path.realpath(__file__)) + '\\64-bit'
if sys.platform.startswith('linux'):
    gSystem = 'linux'

# Core Application Stuff
from PyQt5.QtCore import QCoreApplication, QObject, QUrl, QSize, pyqtSlot
from PyQt5.QtGui import QGuiApplication
# Signals/Slots and other strange definition types
from PyQt5.QtCore import pyqtProperty, pyqtSlot
# Python-Side Widgets
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QTabWidget
# Qml Types
from PyQt5.QtQml import qmlRegisterType, QQmlComponent, QQmlEngine
from PyQt5.QtQuick import QQuickView

# Import the pulse audio wrapper module
#from pulsectl import Pulse


class PTT( QObject ):

    _engine  = None
    _qObject = None

    ## The current state of the microphone
    # False - Muted
    # True  - Active
    _curState = False

    ## Speaker Device
    _speakerDev = 'default'
    _speakerDevId = ''
    ## Speaker Device List
    _speakerList = []
    _speakerIdList = []

    ## Microphone Device
    _micDev = 'default'
    ## Microphone Device List
    _micList = []

    # --- Initialization ---

    def __init__( self, engine, qObject, parent=None):
        super().__init__( parent )

        # Save Params
        self._engine = engine
        self._qObject = qObject

        # Get the device list and set it into the ComboBox
        self.getAudioDevices()                      # Get the list
        self._speakerDev = self._speakerList[0]     # Set the first entry as the device
        self._speakerDevId = self._speakerIdList[0] # Set the first entry as the device
        self._micDev     = self._micList[0]         # Set the first entry as the device
        self._qObject.setProperty('arrAudioDeviceList', self._speakerList)  # Update boxes

        # Connect Slot to signal
        self._qObject.sigPttClicked.connect( self.cbPttActive )
        self._qObject.sigToggleClicked.connect( self.cbToggle )
        self._qObject.sigDeviceChange.connect( self.cbUpdateDevice )

        return

    def __del__( self ):
        self.muteSystemVolume( False )
        self.muteSystemMic( False )
        return

    # -- Control Functions --

    ## \function muteSystemVolume()
    #
    # \param None
    # \return None
    #
    def muteSystemVolume( self, state=False ):
        # Check to make sure that there is a device
        if self._speakerDev == 'None':
            return

        # Setup the command
        cmd = ['SoundVolumeView', '', self._speakerDevId]
        if ( state == True ):
            #call( ['nircmd', 'mutesysvolume', '1'], shell=True )
            cmd[1] = '/Mute'
        else:
            #call( ['nircmd', 'mutesysvolume', '0'], shell=True )
            cmd[1] = '/Unmute'

        # Send the command
        call( cmd, shell=True )
        return

    ## \function muteSystemMic()
    #
    # \param None
    # \return None
    #
    def muteSystemMic( self, state=False ):
        return

    ## \function getAudioDevices()
    #
    # \param None
    # \return None
    #
    def getAudioDevices( self ):
        # Clear the old device lists
        del self._speakerList[:]
        del self._speakerIdList[:]
        del self._micList[:]

        # Start off by creating a list to work with
        filename = 'data.csv'
        call( ['SoundVolumeView', '/scomma', filename], shell=True )

        # Now open the new file
        with open(filename, 'rt') as csvfile:
            reader = csv.reader( csvfile, delimiter=',' )

            # Read Line by line and pick out the microphones and speakers
            for row in reader:
                if row[0] == 'Microphone':
                    self._micList.append( row[3] )
                if row[0] == 'Speakers':
                    self._speakerList.append( row[3] )
                    self._speakerIdList.append( row[14] )

        #END with open...

        # Give the lists some entry if there are no devices
        if len(self._speakerList) == 0:
            self._speakerList.append('None')
            self._speakerIdList.append('None')
        if len(self._micList) == 0:
            self._micList.append('None')

        # Clear out the temporary file
        '''
        if gSystem == 'windows':
            call( ['del', filename] )
        '''

        return

    # --- SLOTS ---

    @pyqtSlot( bool )
    def cbPttActive( self, state ):

        self._curState = state

        # Visual feedback of Microphone state
        if self._curState == True:
            self._qObject.setProperty( 'aliasText', "Pressed...")
            self._qObject.setProperty( 'aliasBoxColor', "green")
        else:
            self._qObject.setProperty( 'aliasText', "Released...")
            self._qObject.setProperty( 'aliasBoxColor', "red")

        self.muteSystemVolume( self._curState )

        return


    @pyqtSlot()
    def cbToggle( self ) :

        self._curState = not self._curState

        if self._curState == True:
            self._qObject.setProperty( 'aliasText', "ON..")
            self._qObject.setProperty( 'aliasBoxColor', "green")
        else:
            self._qObject.setProperty( 'aliasText', "OFF..")
            self._qObject.setProperty( 'aliasBoxColor', "red")

        self.muteSystemVolume( self._curState )

        return

    @pyqtSlot( int )
    def cbUpdateDevice( self, index ):
        self._speakerDev = self._speakerList[index]
        self._speakerDevId = self._speakerIdList[index]
        return

#END class PTT

def main ():

    app = QGuiApplication(sys.argv)

    # === QQmlEngine Style ===
    qmlObj = QQmlEngine()               # Create a QML Engine

    # -- Attach the main window --
    qmlComp = QQmlComponent( qmlObj )   # Create a Component to hold the UI
    qmlComp.loadUrl(QUrl('MainUI.qml')) # Load the QML UI
    qObj = qmlComp.create()             # Get the object from the component

    pttObj = PTT( engine=qmlObj, qObject=qObj )

    return app.exec_()



if __name__ == '__main__':

    sys.exit( main() )
