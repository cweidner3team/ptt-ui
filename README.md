# Python Push To Talk

Using Python as the computer-side interface to controlling system microphone stauts based on external controls.
This program is designed to connect to external hardware through a serial COM port to get PTT events.
An example of such external device would be a footswitch with momentary buttons.

The user interface utilizes Qt through PyQt5 as the GUI frontend.

The "backend" Windows controls to the system sounds utilizes some executables from NirSoft.

## Pre-Requisites

- Python 3.5
- Python Modules:
    - PyQt5

## To Use

Start the program by running `main.py`.

```
python main.py
```

Select the device you want to control with the Combo Box showing device names.

_NOTE: At the moment these devices are speakers instead of microphones_.

Buttons are provided to allow for testing of functionality without external hardware.



